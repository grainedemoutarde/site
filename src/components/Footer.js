import * as React from "react";
import { Link } from "gatsby";

import logo from "../img/logo.svg";
import email from "../img/social/email.svg";
import facebook from "../img/social/facebook.svg";
// import instagram from "../img/social/instagram.svg";
// import twitter from "../img/social/twitter.svg";
// import vimeo from "../img/social/vimeo.svg";

const Footer = () => {

    return (
      <footer className="footer has-background-black has-text-white-ter">
        <div className="content has-text-centered">
          <img
            src={logo}
            alt="Graine de moutarde"
            style={{ width: "14em", height: "10em" }}
          />
        </div>
        <div className="content has-text-centered has-background-black has-text-white-ter">
          <div className="container has-background-black has-text-white-ter">
            <div style={{ maxWidth: "100vw" }} className="columns">
              <div className="column is-4">
                <section className="menu">
                  <ul className="menu-list">
                    <li>
                      <Link className="navbar-item" to="/about/">
                        Le fonds de dotation
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/blog/">
                        Actualités
                      </Link>
                    </li>
                    <li>
                      <a
                        className="navbar-item"
                        href="/admin/"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Admin
                      </a>
                    </li>
                  </ul>
                </section>
              </div>
              <div className="column is-4">
                <section>
                  <ul className="menu-list">
                    <li>
                      <Link className="navbar-item" to="/contribute/">
                        Contribuer
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/contact/">
                        Nous contacter
                      </Link>
                    </li>
                  </ul>
                </section>
              </div>
              <div className="column is-4 social">
                <a title="facebook" href="https://www.facebook.com/fondsgrainedemoutarde">
                  <img
                    src={facebook}
                    alt="Facebook"
                    style={{ width: "1em", height: "1em" }}
                  />
                </a>
                <a title="Email" href="mailto:Fonds de dotation Graine de Moutarde <graine-de-moutarde@arree.bzh>">
                  <img
                    src={email}
                    alt="Email"
                    style={{ width: "1em", height: "1em" }}
                  />
                  <span style={{ padding: "0.5em", color: "#5f504e" }}>
                  graine-de-moutarde@arree.bzh
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
};

export default Footer;
