---
templateKey: 'contribute-page'
path: /contribute/
title: Passons à l'Offensive
image: /img/contribute-banner.jpg
heading: >
 Dons, legs, donations...
description: >
  finançons et auto-gérons les communs de la révolution écologique et sociale !
intro:
  blurbs:
    - image: /img/sortir-capitalisme.jpg
      text: >
        Donner au fonds de dotation c'est faire avancer de façon irrévocable
        la révolution écologique par et pour les habitant·es.

        En effet, de par ses statuts, les biens acquis par le fonds ne pourrons
        jamais être réintégrés dans le système marchand capitaliste.
    - image: /img/reduction-impots.png
      text: >
        Et bien sûr, si vous êtes imposable, 66% du montant de vos dons sont déductibles.
        Ne laisser pas l'État redistribuer vos impôts aux grandes entreprises !
        En donnant au fonds vous avez la garantie d'une utilisation réellement
        démocratique du produit de vos dons.
    - image: /img/heritage.jpg
      text: >
        Legs et donations sont également possibles, et même encouragés.
        L'héritage est un mécanisme par lequel le capitalisme se perpétue.
        Cassons cette logique mortifère en utilisant les legs et donations
        au fonds de dotation.
    - image: /img/antifascisme-communalisme.png
      text: >
        Contribuer ne se résume pas à donner, bien au contraire.
        Nous invitons chacun et chacune à défendre activement leur intérêts
        face à toutes les oppressions et toutes les autorités.
        Agissons ici et maintenant, en rejoignant ou en créant, des collectifs
        s'entraidant et s'organisant en confédération communaliste.

main:
  heading: Un fonds au service des initiatives locales
  description: >
    Le fonds de dotation est piloté par des collectifs locaux partageant ses valeurs, il participe à la réalisation de leurs objectifs, en particulier par la création de cagnottes dédiés (dans la limite des objectifs statutaires du fonds).

    Le conseil d'administration du fonds veille à ce que les biens confiés par le fonds soient effectivement gérés en conformité avec sa charte. Un comité consultatif issu des collectifs locaux donne annuellement son avis et ses préconisation sur la gestion du fonds.

  image1:
    alt: GdM
    image: /img/logo.svg
  image2:
    alt: GdM
    image: /img/logo.svg
  image3:
    alt: GdM
    image: /img/logo.svg
testimonials:
full_image: /img/contribute-offensive.jpg
---
# Pour une confédération communaliste

Le fonds est en particulier un outils au service de collectifs se retouvant dans des valeurs communes et choisissant de s'entraider, de se fédérer, dans tous les domaines de la société : associatifs, médiatiques, politiques, culturelles, sportifs, lieux autogérés mais aussi des lieux de productions (Scop, Scic...) autour de principes fondamentaux que sont :

### Écologie sociale

  Engagement fort pour la défense de l’environnement et la rupture avec le système qui le détruit, gestion égalitaire et démocratique des moyens de production dans le respect des écosystèmes, partage équitable des richesses et des ressources naturelles.

### Démocratie directe

  Autogestion intégrale par la base sur le principe du 1 personne = 1 voix et du mandatement révocable.

### Féminisme

  Au minimum 50% de femmes pour chaque mandat, lutte contre le patriarcat, respect des orientations sexuelles et du genre de chaque individu, mise en œuvre de l’égalité des revenus à travail égal.

### Respect des diversités et des minorités ethniques, religieuses, culturelles ou régionales.

  Engagement dans la lutte contre toutes les formes de racisme et de xénophobie : islamophobie, antisémitisme…

### Fédéralisme global

  Coopération et solidarité entre les structures, respect de l’autonomie décisionnelle, culturelle et économique des autres structures membres de la confédération.

### Ouverture et massification

  Engagement à favoriser la participation et l’adhésion la plus large possible au nouveau modèle de société, pragmatisme, refus des postures de « pureté idéologique », refus des chapelles doctrinaires, attachement à la création d’une unité d’action en dépit des différences des participant·e·s.

### Refus de toute forme d’autoritarisme et du fascisme

### Approche scientifique et rationnelle
  S’attacher la pratique d’une démarche scientifique, privilégier l’établissement et l’analyse des faits, démocratiser la technologie et l’utiliser comme instrument d’émancipation plutôt que d’asservissement, la mettre au service du bien commun plutôt qu’à celui de quelques milliardaires, l’utiliser pour réduire le temps de travail et sa pénibilité.

## Rejoindre une organisation communaliste

Aujourd'hui une initiative fédératrice basée sur nos valeurs est en particulier portée par [L’Offensive](https://offensive.eco/) :
 > L’Offensive est une nouvelle initiative politique en rupture avec toutes formes d'organisations traditionnelles faisant le constat du désastre écologique en cours causé par les plus riches, alimenté par cette mascarade qu’est la «démocratie représentative» qui ne garantie aucunement l’égalité. Elle alimente, accroît et multiplie les formes d’exploitations et ses politiciens sont au service de la bourgeoisie. Nous ne pensons donc pas que le capitalisme soit réformable. N'attendons pas l'hypothétique grand soir, organisons-nous dès maintenant !

 D'autre organisations sont également très proches des valeurs portées par le fonds :

 - La confédération [PEPS - Pour une Écologie Populaire et Sociale](https://confpeps.org/)
 - [Nantes en commun·e·s](https://www.nantesencommun.org/)
