---
templateKey: blog-post
title: "arree.bzh et are.bzh : noms de domaine en commun"
date: 2022-11-01T13:12:00.000Z
featuredpost: false
featuredimage: /img/chatons.png
description: "Nom de domaine et email pour les collectifs de l'Arrée."
tags:
  - arree
  - bzh
  - nom de domaine
  - email
---

### .bzh ou le prix de la vanité...

Un nom de domaine en `.bzh` coûte à minima 41,50€ par an (sans hébergement web ni email).
C'est un coût non négligeable pour des petites organisations qui, à raison, préféreront s'en passer.

### Mais si on partage ?

Le fonds de dotation Graine de Moutarde a réservé les noms de domaine `arree.bzh` et `are.bzh` et propose de les mutualiser.

Vous pouvez ainsi avoir accès gratuitement à un sous-domaine dédié à votre organisation :

 - Un email de type `<orga>@arree.bzh` ou `<orga avec un nom breton>@are.bzh` (ou même `contact@<orga>.arree.bzh` si vous gérez vous même l'hébergement)
 - l'adresse `http://<orga>.arree.bzh` pour votre site internet
 - listes de diffusion en `@listes.arree.bzh` ou `@rollou.are.bzh`.

### Conditions

Votre collectif, association, coopérative, doit avoir son activité principalement dans les monts d'Arrée et respecter deux principes de base :

 - **Écologique** : votre action respecte les écosystèmes et s'inscrit en rupture avec le système qui les détruit.
 - **Démocratique** : votre organisation n'a pas de structure de pouvoir hiérarchique.

Si c'est le cas, [envoyez nous un mail](mailto:graine-de-moutarde@arree.bzh) en décrivant vos besoins (email, nom de domaine, listes de diffusion).

### Techniquement

Les boites mails et listes de diffusions sont hébergées par l'hébergeur associatif brestois [Infini](https://infini.fr).
