---
templateKey: blog-post
title: Premier achat de terre à Berrien !
date: 2021-11-30T13:12:00.000Z
featuredpost: false
featuredimage: /img/Quinoualch.png
description: "Autour du village de Quinoualc'h :"
tags:
  - quinoualch
  - berrien
  - terres
  - forêts
  - landes
---
![](/img/Quinoualch.png)

C'est donc près 5 hectares de terres, forêts et landes qui ont été définitivement rendues aux habitants de l'Arrée !

La gestions de ces terres sera confiée dès que possible à une association partageant les valeurs du fonds.

En attendant, le conseil d'administration du fonds de dotations à décider de louer la partie cultivable de ces terres (environ 2.5 ha) au paysan boulanger du village de Quinoualc'h (bail rural de 9 ans, avec clause environnementale).
