---
templateKey: blog-post
title: Des filtres contre la pénurie d'eau potable
date: 2022-09-17T13:12:00.000Z
featuredpost: true
featuredimage: /img/berrien-carrière-SVT-L.-Gueumé.jpg
description: "À Berrien, l'eau du réseau, puisée dans l'ancienne carrière de kaolin, n'est plus potable. Le fonds organise une commande en gros de systèmes de filtration efficaces. Les inscriptions pour la distribution (rétrocession à prix coutant) sont ouverte !"
tags:
  - berrien
  - scrignac
  - braspart
  - eau
  - potable
  - arsenic
---
![](/img/berrien-carrière-SVT-L.-Gueumé.jpg)

**[Lien pour s'inscrire à un créneau de distribution](https://www.helloasso.com/associations/graine-de-moutarde-fonds-de-dotation/evenements/sytemes-et-cartouches-de-filtration-d-eau-distrubution-et-montage)**

Le réchauffement climatique n'épargne pas les monts d'Arrée. Sur le moyen et sur le long terme, notre territoire doit, comme tous les autres, s'adapter à des conditions climatiques changeantes.

Nous devons mettre en œuvre toutes les pratiques de gestion bocagères, forestières et agroécologiques qui assureront la survie du vivant malgré des sécheresses de plus en plus importantes. La réduction de notre consommation d'eau est également vitale : toutes nos pratiques sont à remettre en question : élevage hors-sol, chasse-d'eau-potable, piscine, tourisme, etc. Nous invitons les habitant·es des monts d'Arrée à se saisir de ce sujet critique, et à exiger un contrôle démocratique sur les ressources en eau.

Depuis fin juillet et encore aujourd'hui, beaucoup d'habitant·es, notamment à Berrien et Scrignac, n'ont plus d'eau potable au robinet. Cette situation peut mettre en danger, en particulier les personnes vulnérables ou en situation précaire. Les pollutions de l'eau du réseau à Berrien sont à la fois bactériologique et due à des taux d'arsenic élevés.

**Pour l'eau polluée à l'arsenic (entre autres), il ne suffit pas de la faire bouillir pour la rendre potable : il faut une filtration spéciale, qui peut être coûteuse.**
Et même pour les pollutions uniquement bactériologiques, faire bouillir l'eau coûte cher, et de plus en plus avec la hausse des prix de l'électricité.

### Des systèmes de filtration par gravité, efficaces et abordables

C'est pourquoi le fonds Graine de Moutarde va proposer aux habitant·es, des systèmes de filtration par gravité permettant de potabiliser l'eau. Ces systèmes seront proposés à prix coûtant + une participation libre afin de subventionner l'achat pour les individu·es ayant moins de moyens financiers.

<p align=center>
<a href="https://www.youtube.com/watch?v=T2k31DAoFOs&amp;list=PLEcrma4Txzmzg1NH4K_mblqb4I534yVa2"><img src="https://img.youtube.com/vi/ZaZGaDFo2aY/0.jpg" alt="Présentation de l'initative en image"></a>
<br />
Présentation de l'initative et instruction de montage sur notre chaine Youtube.
</p>

Ces systèmes seront auto-construits à partir de [2 seaux de 10 litres transparents (plastique alimentaire)](https://www.alibaba.com/product-detail/Food-Grade-Transparent-Clear-Plastic-Bucket_1600511166411.html), [un robinet vanne](https://www.alibaba.com/product-detail/Water-Bibcock-Faucet-Golden-Tap-Garden_1600483167915.html), [1 cartouche de charbon actif/micro-membrane](https://www.alibaba.com/product-detail/Replacement-Filter-Cartridge-Compatible-with-BB9_1600060527285.html) et [1 cartouche de filtration de l'arsenic (alumine activée)](https://www.alibaba.com/product-detail/Portable-travel-gravity-purification-system-PF_1600315780684.html).
Pour l'appel d'air : [34 cm de tube silicone 5x7mm](https://fr.aliexpress.com/item/1005004297786368.html) associé à [petit connecteur 4.8-G1-4.8](https://fr.aliexpress.com/item/1005001581620493.html) et un joint caoutchouc 12x17.

Afin de rendre le système abordable nous avons passé commande directement aux fabricants (en Chine, il n'y malheureusement pas plus proche).

![L'eau contaminée passe par le filtre à charbon/micro-membrane puis par le filtre spécial arsenic](/img/schema-filtration.jpeg)

On rempli le seau du haut avec de l'eau contaminée (arsenic, bactérie, etc.) et on récupère de l'eau potable dans le seau du bas. C'est aussi simple que ça !

À noter que vous pouvez également utiliser vos propres récipients (fait-tout inox par exemple). Seule contrainte : la hauteur de la cartouche charbon-actif, 22 cm, qui doit rentrer entièrement dans le récipient supérieur sans toucher le couvercle. Pour le récipient du bas, la cartouche spécial arsenic a elle une hauteur de 16 cm.

### Les analyses

[Nous avons testé la cartouche de filtration spécial arsenic (générique PF-2)](/files/Résultat-analyse-arsenic-filtres.pdf) : **le taux d'arsenic est réduit d'environ 75%, passant de 7,8 µg/L (dans l'eau du réseau) à 2 µg/L.** (en comparaison, la cartouche Ultra Sterasyl de Doulton ne filtrait que 20% de l'arsenic).

Nous avons également vérifié l'efficacité de la filtration des bactéries et autres contaminants par la cartouche à charbon-actif (générique BB9-2). Voici les analyses :
 - eau de pluie [avant filtration](/files/Résultat-analyse-eau-pluie-avant-filtration.pdf);
 - eau de pluie [après filtration](/files/Résultat-analyse-eau-pluie-apres-filtration.pdf).

Ces analyses ont été effectuées après environ 1 mois d'utilisation des cartouches.
Seul inconvénient des cartouches charbon actif : elles ont tendence à augmenter le pH (+basique/alcalin) de l'eau lors des premières semaines d'utilisation.

#### Comment savoir si vous avez besoin d'un filtre pour l'arsenic ?

La limite légale pour l'arsenic est de 10µg/L. Cependant, l'arsenic étant cancérigène, il n'y a pas d'effet de seuil : plus le taux est bas, mieux c'est, tout en prenant également en compte les coûts associés pour réduire ce risque.

L'[OMS estime](https://extranet.who.int/iris/restricted/bitstream/handle/10665/66326/WHO_SDE_WSH_00.4.pdf) que consommer toute sa vie une eau avec un taux d'arsenic à 10µg/L pose un sur-risque de 6 "chances" sur 10 000 de développer un cancer de la peau (attribuable à l'arsenic) au cours de sa vie.

**Vous pouvez vérifier le taux d'arsenic dans votre commune** sur cette [page de l'ARS](https://orobnat.sante.gouv.fr/orobnat/afficherPage.do?methode=menu&usd=AEP&idRegion=53).

NB : Il faut parfois naviguer plusieurs fois vers les bulletins précédents pour trouver une analyse du taux d'arsenic.

On peut estimer qu'un taux d'arsenic habituel dans l'eau potable inférieur à 4µg/L ne nécessite probablement pas de cartouche de filtration de l'arsenic. Mais c'est à chacun·e de décider selon sa situation personnelle.

### Mise en service et entretien

Lors de la mise en service, il faut amorcer au robinet les cartouches, en particulier [la cartouches spécial arsenic](https://www.youtube.com/watch?v=sHh9Q4St5k4), puis jeter les 10 premiers litres d'eau filtrés.

Pensez également à [nettoyer régulièrement](https://www.youtube.com/watch?v=ObD7TYcBL90) (au moins tous les mois) la cuve du bas, et la cartouche carbon-actif lorsqu'elle commencer à s'encrasser.

### Coût et durée de vie

Le fonds de dotation ne fait aucun bénéfice, tous les éléments constitutifs du système sont rétrocédés à prix coûtant :

 - 1 cartouche au charbon actif (type BB9-2) : 8.50€
 - 1 cartouche spécial arsenic (type PF-2): 4.00€
 - 2 seaux de 10L (plastique alimentaire transparent PP) : 10.00€
 - 1 robinet : 1.00€
 - petits accessoires (appel d'air, étiquette...) : 1.00€

**Montant total du kit : 25,00 euros !** (dont 0.5€ pour mutualisation du risque de casse des seaux en plastique lors du perçage)

Ces prix correspondent à ceux obtenus début octobre 2022 et sont susceptibles de varier en fonction de l'inflation.

Une cartouche charbon-actif permet de filtrer environ 11 350 litres d’eau.
La cartouche spéciale arsenic est à remplacer tous les 1 892 litres environ (comme cela dépend du taux d'arsenic de départ, nous ferons des analyses régulièrement pour évaluer la durée de vie réelle).

Pour une consommation de 5 litres par jour (consommation estimée pour 1 personne), cela revient à changer le filtre à charbon actif tous les 6 ans et le filtre arsenic tous les ans.

**Ce système permet donc d'obtenir une eau potable pour un surcoût d'environ 0,4 centime par litre**.

À titre de comparaison, faire bouillir 1 litre d'eau avec une bouilloire électrique coûte actuellement autour de 3,4 centimes (donc sans filtration de l'arsenic et autres polluants chimiques).

### Distribution des kits de filtration

**150 kits sont disponibles depuis début décembre (priorité au habitant·es sans eau potable au robinet), les ateliers d'autoconstruction sont annoncés sur [Facebook](https://www.facebook.com/fondsgrainedemoutarde), [Mastodon](https://piaille.fr/@grainedemoutarde) et sur [la lettre d'information du collectif « Eau Berrien »](https://listes.infini.fr/listes.arree.bzh/subscribe/eau-berrien-infos)**

**Si vous avez des questions, [laissez nous un message](/contact/) !**

Vous pouvez également contacter l'administrateur du fonds en charge de cette initiative, Jean-Baptiste Giraudeau (habitant de Berrien), au 06 77 93 36 60.
