---
templateKey: blog-post
title: Le fonds s'ouvre aux territoires autres que les monts d'Arrée
date: 2024-06-19T14:43:00.000Z
description: Nous avons modifier nos statuts pour refléter notre nouvelle
  ambition de soutenir la création de communs sur tous les territoires, au
  service d'un projet de société communaliste et internationaliste !
featuredpost: true
featuredimage: /img/logo.png
tags:
  - communs
  - statuts
---
Depuis 2021, le fonds était géré par une petite équipe de 3 administrateur.ices dans les monts d'Arrée. De même, par ses statuts, le fonds s'auto-limitait à agir dans les monts d'Arrée afin de garantir un suivi qualitatif des initiatives financées.

Mais cela a changé en 2024 : l'équipe du fonds s'est agrandie géographiquement et compte désormais 8 personnes, dont 3 issues du groupe local de [l'Offensive](https://offensive.eco/) à Lille.

Le fonds a également décidé d'accepter le financement d'initiatives sur tous les territoires, à partir du moment où elles sont portées par des collectifs partageant nos valeurs (écologie sociale, démocratie directe, féminisme, antiracisme, etc.).

Nos statuts ont donc été modifiés pour refléter notre nouvelle ambition de soutenir la création de communs sur tous les territoires, au service d'un projet de société communaliste et internationaliste !

![](/img/VdT.jpg)
Un premier collectif hors des monts d'Arrée, « [Les Vagues de Terres](https://terredivague.live-website.com/) » près de Lannion, a ainsi pu profiter d'une cagnotte dédiée initiée par le fonds afin de récolter des dons défiscalisés pour l'achat de leur lieu collectif (via un apport associatif avec droit de reprise).

Vous aussi vous pouvez dès aujourd'hui [soutenir le financement de futurs communs par vos dons](/contribute/) (défiscalisés à 66%),

et si votre collectif à un besoin d'investissement pour un projet en accord avec [nos statuts](/about/), n'hésitez pas à [nous contacter](/contact/) !
