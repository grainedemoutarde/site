---
templateKey: 'blog-post'
title: 'Création de la coopérative semencière Graines de liberté - Hadoù Ar Frankiz '
date: 2022-01-04T13:12:00.000Z
featuredpost: false
description: >-
  Le fonds de dotation est membre fondateur de la SCIC « Graines de liberté ».
featuredimage: /img/graines-de-liberte.jpg
tags:
  - semences
  - paysannerie
---
![](/img/graines-de-liberte.jpg)

Afin de favoriser l'émergence d'une coopérative semencière paysanne établie dans le Finistère (et même dans toute la Bretagne), le fonds de dotation a libéré 1000 euros pour soutenir la création de la SCIC (Société coopérative d'intérêt collectif) « [Graines de liberté - Hadoù Ar Frankiz](https://www.grainesdeliberte.coop/accueil/) ». Plusieurs habitant·es de monts d'Arrée ont participé à l'aventure, en tant que salarié·es ou bénévoles.

Les semences de la coopérative sont déjà en vente, notamment au marché de Quinoualc'h (Berrien).
