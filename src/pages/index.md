---
templateKey: index-page
title: Graine de Moutarde
image: /img/fond.jpg
heading: Des terres et des lieux collectifs
subheading: Les communs de la révolution écologique et sociale
mainpitch:
  title: Un fonds de dotation au service des habitant·es
  description: >
    Le fonds « Graine de Moutarde » reçoit ou achète des terres, forêts et bâtis
    puis en confie la gestion à des collectifs d'habitant·es.
    Les communs ainsi créés sont définitivement préservés du système marchand capitaliste.
    Le fonds finance également des actions d’entraide et contribue à la création de coopératives.
description: >-
    Le fonds de dotation « Graine de Moutarde » s’engage pour la protection et le développement de territoires vivants, habités, libres de toute exploitation et oppression, en soutien aux collectifs partageant nos valeurs. L’objectif du fonds est de favoriser l’émergence, le développement, la mise en réseau, et le maintien de projets allant dans le sens d’une protection de la biodiversité et du climat, de la promotion de l’agriculture paysanne et du soutien aux projets défendant les valeurs d’utilité sociale, de démocratie directe et de solidarité dans l’ensemble des domaines de l’activité humaine.
intro:
  blurbs:
    - image: /img/ecosocialism-tree-fist.jpg
      text: >
        Écologique : Les terres et forêts doivent être gérés afin de favoriser la régénération de la biodiversité ainsi que l'adaptation au changement climatique.
    - image: /img/democracie-directe.png
      text: >
        Démocratique : tous les biens confiés par le fonds sont gérés par des associations fonctionnant sur le principe de la démocratie directe : 1 personne = 1 voix et mandats révocables.
---
