---
templateKey: about-page
path: /about/
title: Pourquoi un fonds de dotation ?
---
# Notre survie et celles de nos éco-systèmes sont en jeu

Le fonds de dotation « Graine de Moutarde » est un outil au service de l'émancipation des habitant·es. Notre survie passe en effet par la réappropriation de nos moyens de subsitance, la création de communs à la base d'une solidarité forte et l'auto-gestion démocratique de notre environnement.

Tout celà requiert d'abattre le capitalisme, mais pour cela il faut démontrer qu'une autre façon de faire société est possible et enviable.
Le fonds de dotation a pour but de financer les premiers communs de cette société nouvelle.

# Statuts

Né en 2021 dans les monts d'Arrée, le fonds de dotation « Graine de Moutarde » s’engage pour la protection et le développement de territoires vivants, habités, libres de toute exploitation et oppression, en soutien aux collectifs partageant nos valeurs. L’objectif du fonds est de favoriser l’émergence, le développement, la mise en réseau, et le maintien de projets allant dans le sens d’une protection de la biodiversité et du climat, de la promotion de l’agriculture paysanne et du soutien aux projets défendant les valeurs d’utilité sociale, de démocratie directe et de solidarité dans l’ensemble des domaines de l’activité humaine.

Il s’inscrit dans une mission d’intérêt général.

Pour cela, le fonds œuvre, entre autres, sur plusieurs plans

 - à respecter, entretenir et préserver les divers milieux naturels existants - les prairies, les zones humides, les milieux aquatiques, les forêts - en favorisant la diversité et la continuité, en excluant les pratiques et intrants toxiques pour l’équilibre des écosystèmes et du climat (pesticides, engrais polluants, coupes rases, labour profond, destruction de talus, fermes-usines, équipements industriels néfastes, etc.) et limitant le recours aux énergies fossiles ;

 - à encourager les cultures agricoles locales, l’installation de projets d’agriculture paysanne, le développement de projets vivriers et la mise en place de circuits courts, quatre éléments essentiels à la conservation des sols et des paysages ruraux ;

 - à encourager la création et le développement de sociétés coopératives et participatives (SCOP, SCIC, SAPO, CAE...)

 - à soutenir les initiatives, les expérimentations locales, la préservation et le développement du lien social et des solidarités entre les projets et entre les générations, la valorisation d’une cohérence territoriale au niveau écologique, social et économique, dans l’idée d’une relocalisation des activités et des biens ;

 - à valoriser la construction et l’entretien d’habitats traditionnels, alternatifs et innovants à faible impact écologique, notamment par l’utilisation de matériaux d’origine locale, ou de techniques d’auto-construction, permettant aux personnes ayant des activités sur le territoire d’y vivre et d’y être attachées sensiblement ;

 - à promouvoir les pratiques de formation mutuelle, de coopération, de mise en commun des outils et des activités, et plus globalement à développer les rapports sociétaux alternatifs et collectifs allant dans le sens du bien commun ;

 - à travailler à la restauration, à la préservation et à la diffusion du patrimoine culturel, notamment par la construction d’espaces de rencontre, de formation ou d’éducation populaire allant dans le sens d’une plus grande participation des habitant·es aux enjeux soulevés.

 - à favoriser la résilience du territoire (capacité à revenir rapidement sur un fonctionnement normal en cas de choc important et prolongé), par le développement de technologies et capacités de production appropriées (réparables localement), au service de tous et toutes.

## Éthique scientifique

Le fonds « Graine de Moutarde » s’oblige, pour ses prises de décisions, la réalisation de ses missions et l’évaluation de ses actions, à se baser sur les acquis et consensus scientifiques.

De même le fonds s’engage à ne pas promouvoir de doctrines sectaires, ésotériques ou pseudo-scientifiques.

## Modalités de mises à disposition des biens du fonds ou prise de participations

Les collectifs auxquels le fonds confie l’usage de ses biens ou dont le fonds participe au financement, doivent respecter, dans la mesure du possible, la charte constituée des principes structurants suivants :

- - -

### Écologie sociale
Engagement fort pour la défense de l’environnement et la rupture avec le système qui le détruit, gestion égalitaire et démocratique des moyens de production dans le respect des écosystèmes, partage équitable des richesses et des ressources naturelles.

### Démocratie directe
Autogestion intégrale par la base sur le principe du 1 personne = 1 voix et du mandatement révocable.

### Féminisme
Au minimum 50% de femmes pour chaque mandat, lutte contre le patriarcat, respect des orientations sexuelles et du genre de chaque individu, mise en œuvre de l’égalité des revenus à travail égal.

### Respect des diversités et des minorités ethniques, religieuses, culturelles ou régionales.
Engagement dans la lutte contre toutes les formes de racisme et de xénophobie : islamophobie, antisémitisme…

### Fédéralisme global
Coopération et solidarité entre les structures soutenues par le fonds ou partageant ses valeurs, respect de l’autonomie décisionnelle, culturelle et économique.

### Ouverture et massification
Engagement à favoriser la participation et l’adhésion la plus large possible au modèle de société promu par cette charte, pragmatisme, refus des postures de « pureté idéologique », refus des chapelles doctrinaires, attachement à la création d’une unité d’action en dépit des différences des participant·e·s.

### Refus de toute forme d’autoritarisme et du fascisme

### Approche scientifique et rationnelle
S’attacher la pratique d’une démarche scientifique, privilégier l’établissement et l’analyse des faits, démocratiser la technologie et l’utiliser comme instrument d’émancipation plutôt que d’asservissement, la mettre au service du bien commun plutôt qu’à celui de quelques milliardaires, l’utiliser pour réduire le temps de travail et sa pénibilité.

- - -

Des clauses d’application pourront être précisés pour chaque bien, ainsi que la manière de les rendre engageantes juridiquement.

La mise à disposition du bien se fait de préférence par bail rural/forestier (avec clauses environnementales) ou commercial le cas échéant, ou sinon par bail emphytéotique. Si ces formes ne conviennent pas à l’une des parties, pour quelconque raison argumentée, alors il pourra être envisagé une mise à disposition par :

 - apport avec droit de reprise dans le cas d’une association loi 1901 ;
 - apport d’usufruit ;
 - ou autre solution conforme au droit.

### Connaissance mutuelle et droit de regard

Afin de s’assurer du respect des statuts et de la charte, il sera prévu des visites régulières par le CA. Cette pratique vise à favoriser l’entraide et la connaissance mutuelle.

Si des pratiques contraires aux statuts ou clauses du bail sont observés une procédure de concertation sera mise en place dès le prochain CA. Il pourra aussi être demandé un CA extraordinaire.

Le fonds de dotation se donne alors le droit, après la mise en place d’un processus de médiation, de :

 - demander le transfert du bail au fonds de dotation ou à un autre individu ou collectif preneur ;
 - reprendre le bien s’il s’agit d’un apport avec droit de reprise ;
 - ne plus octroyer d’appui de quelque façon que ce soit à l'individu ou au collectif concerné.
